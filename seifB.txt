update students set studentAge = 17 where studentName="David" or studentName="Mike";

update students set studentGrade = 93 where studentName="Anat";

insert into students(studentID, studentName, studentAge, studentGrade) values(11, "eden", 16, 100);
insert into students(studentID, studentName, studentAge, studentGrade) values(12, "oriel", 16, 15);
insert into students(studentID, studentName, studentAge, studentGrade) values(13, "youval", 16, 100);

insert into classes(classID, className, teacherID, catID) values(12, "mathClass" ,5, 2);

insert into teachers(teacherID, teacherName) values(8, "adar");

update classes set teacherID = 8 where classID = 12;

insert into classes(classID, className, teacherID, catID) values(13, "education" ,7, 4);

insert into StudentsInClasses(studentID, classID) values(11,13);
insert into StudentsInClasses(studentID, classID) values(12,13);
insert into StudentsInClasses(studentID, classID) values(13,13);

select count(*) from teachers;

select AVG(studentGrade) from students;

